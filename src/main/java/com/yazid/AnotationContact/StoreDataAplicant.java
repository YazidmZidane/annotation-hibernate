package com.yazid.AnotationContact;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;

public class StoreDataAplicant {
	public static void main(String[] args) {
		StandardServiceRegistry ssr=new StandardServiceRegistryBuilder().configure("hibernate.cfg.xml").build();  
		Metadata meta=new MetadataSources(ssr).getMetadataBuilder().build();  

		SessionFactory factory=meta.getSessionFactoryBuilder().build();  
		Session session=factory.openSession();  

		Transaction t=session.beginTransaction();
		
		Aplicant apl1 = new Aplicant();
		apl1.setFirst_name("Yazid");
		apl1.setLast_name("Zidane");
		
		Contact ctc1 = new Contact();
		ctc1.setCity("Bandung");
		ctc1.setNumber("087823289576");
		
		apl1.setContact(ctc1);
		ctc1.setAplicant(apl1);
		
		session.persist(apl1);
		
		t.commit();
		session.close();
		System.out.println("Success");
	}
}
