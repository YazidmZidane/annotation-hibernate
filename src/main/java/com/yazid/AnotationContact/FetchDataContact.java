package com.yazid.AnotationContact;

import java.util.Iterator;
import java.util.List;

import javax.persistence.TypedQuery;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;

public class FetchDataContact {
	public static void main(String[] args) {
		StandardServiceRegistry ssr=new StandardServiceRegistryBuilder().configure("hibernate.cfg.xml").build();  
		Metadata meta=new MetadataSources(ssr).getMetadataBuilder().build();  

		SessionFactory factory=meta.getSessionFactoryBuilder().build();  
		Session session=factory.openSession();  

		TypedQuery query=session.createQuery("from Aplicant");    
		List<Aplicant> list=query.getResultList();    

		Iterator<Aplicant> itr=list.iterator();    
		while(itr.hasNext()){    
			Aplicant s = itr.next();
			Contact c = s.getContact();			
			System.out.println("Aplicant Name : "+ s.getFirst_name()+" "+s.getLast_name());
			System.out.println("Contact Aplicant");
			System.out.println("City : "+c.getCity());
			System.out.println("Number Phone : "+c.getNumber());
		}  
		session.close();    
		System.out.println("success"); 
	}
}
