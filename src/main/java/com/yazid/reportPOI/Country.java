package com.yazid.reportPOI;

public class Country {
	
	private String shortCode;
	private String name;	
	
	public Country() {
		
	}
	
	public Country(String shortCode, String name) {
		this.shortCode = shortCode;
		this.name = name;
	}

	public String getShortCode() {
		return shortCode;
	}

	public void setShortCode(String shortCode) {
		this.shortCode = shortCode;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	@Override
	public String toString() {
		return shortCode + " " + name;
	}
}
