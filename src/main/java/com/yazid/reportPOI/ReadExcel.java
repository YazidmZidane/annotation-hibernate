package com.yazid.reportPOI;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ReadExcel {
	
	public static List<Country> readExcelData(String fileName) {
		List<Country> countriesList = new ArrayList<Country>();
		try {
			FileInputStream fis = new FileInputStream(fileName);
			
			Workbook workBook = null;
			if(fileName.toLowerCase().endsWith("xlsx")) {
				workBook = new XSSFWorkbook(fis);
			} else if (fileName.toLowerCase().endsWith("xls")) {
				workBook = new HSSFWorkbook(fis);
			}
			
			int numberOfSheets = workBook.getNumberOfSheets();
			for(int i=0; i<numberOfSheets; i++) {
				Sheet sheet = workBook.getSheetAt(i);
				Iterator<Row> rowIterator = sheet.iterator();
				
				while(rowIterator.hasNext()) {
					String shortCode = "";
					String name = "";
					Row row = rowIterator.next();					
					Iterator<Cell> cellIterator = row.cellIterator();
					
					while(cellIterator.hasNext()) {
						Cell cell = cellIterator.next();
						
						switch(cell.getCellType()) {
						case Cell.CELL_TYPE_STRING: 
							if(shortCode.equalsIgnoreCase("")) {
								shortCode = cell.getStringCellValue().trim();
							} else if(name.equalsIgnoreCase("")) {
								name = cell.getStringCellValue().trim();
							} else {
								System.out.println("Random : :"+cell.getStringCellValue());
							}
							break;
						case Cell.CELL_TYPE_NUMERIC:
							System.out.println("Random : :"+cell.getNumericCellValue());
						}
					}
					Country c = new Country(shortCode, name);
					countriesList.add(c);
				}
			}
			fis.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
				
		return countriesList;
	}
	
	public static void main(String[] args) {
		String path = System.getProperty("user.dir");
		List<Country> list = readExcelData(path+"\\Excel File\\Sample.xlsx");
		System.out.println("Country List\n"+list);
	}
}
