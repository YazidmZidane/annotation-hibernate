package com.yazid.AnotationEmployee;

import java.util.Iterator;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;

public class StoreData {    
	public static void main(String[] args) {    

		StandardServiceRegistry ssr = new StandardServiceRegistryBuilder().configure("hibernate.cfg.xml").build();  
		Metadata meta = new MetadataSources(ssr).getMetadataBuilder().build();  

		SessionFactory factory = meta.getSessionFactoryBuilder().build();  
		Session session = factory.openSession();  
		Transaction t = session.beginTransaction();
		
		Session sessionoutput = factory.openSession();

		Employee e1=new Employee();        
		e1.setFirst_name("Yazid");    
		e1.setLast_name("Zidane");
		e1.setSalary(5000000);

		session.save(e1);  
		t.commit();  
		System.out.println("successfully saved");
		try {
			 
	         List employees = sessionoutput.createQuery("FROM Employee").list(); 
	         for (Iterator iterator = employees.iterator(); iterator.hasNext();){
	            Employee employee = (Employee) iterator.next(); 
	            System.out.print("First Name: " + employee.getFirst_name()); 
	            System.out.print("  Last Name: " + employee.getLast_name()); 
	            System.out.println("  Salary: " + employee.getSalary()); 
	         }
	      } catch (HibernateException e) {
	         if (t!=null) t.rollback();
	         e.printStackTrace(); 
	      } finally {
	         sessionoutput.close(); 
	      }
		factory.close();  
		session.close();    

	}    
}   