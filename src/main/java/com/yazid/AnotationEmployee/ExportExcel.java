package com.yazid.AnotationEmployee;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;

import javax.persistence.TypedQuery;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;

public class ExportExcel {
	public static void main(String[] args) throws IOException {
		StandardServiceRegistry ssr = new StandardServiceRegistryBuilder().configure("hibernate.cfg.xml").build();  
		Metadata meta = new MetadataSources(ssr).getMetadataBuilder().build();  

		SessionFactory factory = meta.getSessionFactoryBuilder().build();  
		Session session = factory.openSession();  
		
		TypedQuery query=session.createQuery("from Employee");    
	    List<Employee> list=query.getResultList();

		XSSFWorkbook workbook = new XSSFWorkbook(); 
		XSSFSheet spreadsheet = workbook.createSheet("Employe_DB");

		XSSFRow row = spreadsheet.createRow(1);
		XSSFCell cell;
		cell = row.createCell(1);
		cell.setCellValue("id");
		cell = row.createCell(2);
		cell.setCellValue("first_name");
		cell = row.createCell(3);
		cell.setCellValue("last_name");
		cell = row.createCell(4);
		cell.setCellValue("salary");
		int i=2;
		
		Iterator<Employee> itr = list.iterator();
		while(itr.hasNext()) {
			Employee employee = itr.next(); 
			row = spreadsheet.createRow(i);
			cell = row.createCell(1);
			cell.setCellValue(employee.getId());
			cell = row.createCell(2);
			cell.setCellValue(employee.getFirst_name());
			cell = row.createCell(3);
			cell.setCellValue(employee.getLast_name());
			cell = row.createCell(4);
			cell.setCellValue(employee.getSalary());
		}
		
		String path = System.getProperty("user.dir");
		FileOutputStream fos = new FileOutputStream(new File(path+"\\Excel File\\TestEmployee.xlsx"));
		workbook.write(fos);
		fos.close();
		System.out.println("Success");
	}
}
