package com.yazid.AnotationEmployee;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Iterator;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;

public class ImportExcel {
	public static void main(String[] args) throws IOException {
		StandardServiceRegistry ssr = new StandardServiceRegistryBuilder().configure("hibernate.cfg.xml").build();  
		Metadata meta = new MetadataSources(ssr).getMetadataBuilder().build();  

		SessionFactory factory = meta.getSessionFactoryBuilder().build();  
		Session session = factory.openSession();  
		Transaction t = session.beginTransaction();
		
		String path = System.getProperty("user.dir");
		FileInputStream fis = new FileInputStream(new File(path+"\\Excel File\\TestEmployee.xlsx"));
		Workbook workbook = new XSSFWorkbook(fis);
		XSSFSheet spreadsheet = (XSSFSheet) workbook.getSheet("Employe_DB");
		XSSFRow row;
		XSSFCell cell;
		Iterator <Row>  rowIterator = spreadsheet.iterator();
		int i = 2;
		while(rowIterator.hasNext()) {
			Employee emp = new Employee();
			row = spreadsheet.getRow(i);
			if (row == null) {
				break;
			}
			cell = row.getCell(1);
			emp.setId((int) cell.getNumericCellValue());
			cell = row.getCell(2);
			emp.setFirst_name(cell.getStringCellValue().trim());
			cell = row.getCell(3);
			emp.setLast_name(cell.getStringCellValue().trim());
			cell = row.getCell(4);
			emp.setSalary((int) cell.getNumericCellValue());
			session.save(emp);  			
			i++;
		}
		t.commit();
		fis.close();
		factory.close();
		session.close();
		System.out.println("Success");
	}		 
}
