package com.yazid.AnotationStudent;

import java.util.Iterator;
import java.util.List;

import javax.persistence.TypedQuery;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;

public class FetchDataStudent {
	public static void main(String[] args) {
		StandardServiceRegistry ssr=new StandardServiceRegistryBuilder().configure("hibernate.cfg.xml").build();  
		Metadata meta=new MetadataSources(ssr).getMetadataBuilder().build();  

		SessionFactory factory=meta.getSessionFactoryBuilder().build();  
		Session session=factory.openSession();  

		TypedQuery query=session.createQuery("from Schedule");    
		List<Schedule> list=query.getResultList();    

		Iterator<Schedule> itr=list.iterator();    
		while(itr.hasNext()){    
			Schedule s = itr.next();
			Student student = s.getStudents();
			Classes classes = s.getClasses();
			System.out.println("Student Name : "+ student.getFirst_name()+" "+student.getLast_name());
			System.out.println("Class Name : "+classes.getClass_name()+" Description : "+classes.getDescription());   
			System.out.println("=================================================================================");
		}  
		session.close();    
		System.out.println("success"); 
	}
}
