package com.yazid.AnotationStudent;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;

public class StoreDataStudent {
	public static void main(String[] args) {
		StandardServiceRegistry ssr=new StandardServiceRegistryBuilder().configure("hibernate.cfg.xml").build();  
		Metadata meta=new MetadataSources(ssr).getMetadataBuilder().build();  

		SessionFactory factory=meta.getSessionFactoryBuilder().build();  
		Session session=factory.openSession();  

		Transaction t=session.beginTransaction();		

		Student stu1 = new Student();
		stu1.setFirst_name("Yazid");
		stu1.setLast_name("Zidane");

		Student stu2 = new Student();
		stu2.setFirst_name("Thoriq");
		stu2.setLast_name("Sulthan");

		Classes clas1 = new Classes();
		clas1.setClass_name("RS01");
		clas1.setDescription("Ruangan Komputer");

		Classes clas2 = new Classes();
		clas2.setClass_name("RS02");
		clas2.setDescription("Lab Kimia");		

		Classes clas3 = new Classes();
		clas3.setClass_name("RS03");
		clas3.setDescription("Ruangan Kelas");
		
		Schedule sch1 = new Schedule();
		sch1.setStudents(stu1);
		sch1.setClasses(clas1);
		
		Schedule sch2 = new Schedule();
		sch2.setStudents(stu1);
		sch2.setClasses(clas3);
		
		Schedule sch3 = new Schedule();
		sch3.setStudents(stu2);
		sch3.setClasses(clas2);
		
		Schedule sch4 = new Schedule();
		sch4.setStudents(stu2);
		sch4.setClasses(clas3);
		
		session.persist(sch1);
		session.persist(sch2);
		session.persist(sch3);
		session.persist(sch4);

		t.commit();
		session.close();
		System.out.println("Success");
	}
}
