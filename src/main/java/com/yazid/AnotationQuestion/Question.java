package com.yazid.AnotationQuestion;

import java.util.List;

import javax.persistence.*;

@Entity
@Table(name = "question")
public class Question {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	@Column(name="questionName")
	private String questionName;
	
	@OneToMany(cascade=CascadeType.PERSIST)
	@JoinColumn(name="qid")	 
	@OrderColumn(name="type")  
	private List<Answer> answer;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getQuestionName() {
		return questionName;
	}

	public void setQuestionName(String questionName) {
		this.questionName = questionName;
	}

	public List<Answer> getAnswer() {
		return answer;
	}

	public void setAnswer(List<Answer> answer) {
		this.answer = answer;
	}		
}
