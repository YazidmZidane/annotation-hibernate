package com.yazid.AnotationQuestion;

import java.util.*;  
import javax.persistence.TypedQuery;  
import org.hibernate.*;  
import org.hibernate.boot.Metadata;  
import org.hibernate.boot.MetadataSources;  
import org.hibernate.boot.registry.StandardServiceRegistry;  
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;   
    
public class FetchDataQuestion {    
public static void main(String[] args) {    
        
    StandardServiceRegistry ssr=new StandardServiceRegistryBuilder().configure("hibernate.cfg.xml").build();  
    Metadata meta=new MetadataSources(ssr).getMetadataBuilder().build();  
      
    SessionFactory factory=meta.getSessionFactoryBuilder().build();  
    Session session=factory.openSession();  
        
    TypedQuery query=session.createQuery("from Question");    
    List<Question> list=query.getResultList();    
        
    Iterator<Question> itr=list.iterator();    
    while(itr.hasNext()){    
        Question q=itr.next();    
        System.out.println("Question Name: "+q.getQuestionName());
        System.out.println("Answer : ");
            
        //printing answers    
        List<Answer> list2=q.getAnswer();    
        Iterator<Answer> itr2=list2.iterator();    
       while(itr2.hasNext())  
       {  
        Answer a=itr2.next();  
            System.out.println(a.getAnswername()+"! Answer By : "+a.getPostedBy());  
        }    
    }  
    session.close();    
    System.out.println("success");    
}    
}    